package cest.edu.aval2;

public class IPessoa {
	
	public String nome;
	public String CPF;
	public String telefone;
	public String endereço;
	public String email;
	
	//construtores 
	public IPessoa(String nome, String CPF, String telefone, String endereço, String email) {
		this.nome = nome;
		this.CPF = CPF;
		this.telefone = telefone;
		this.endereço = endereço;
		this.email = email;
	}
	
	public IPessoa(String nome, String telefone, String endereço, String email) {
		this.nome = nome;
		this.telefone = telefone;
		this.endereço = endereço;
		this.email = email;	
	}	
	
	//getters
	public String getNome() {
		return nome;
	}

	public String getCPF() { 
		return CPF;
	}
	public String getEndereço() {
		return endereço;
	}
	public String getTelefone() {
		return telefone;
	}
	public String getEmail() {
		return email;
	}
	
	//setters
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCPF(String CPF) {
		this.CPF = CPF;
	}
	public void setEndereço(String endereço) {
		this.endereço = endereço;
	}	
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}

