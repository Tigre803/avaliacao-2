package cest.edu.aval2;

public class Cidade{
	
	private String nome;
	private String UF;
	
	//getters
	public String getNome() {
		return nome;
	}
	
	public String getUF() {
		return UF;
	}
	
	//setters
	public void setNome(String nome) {
		this.nome = nome; 
	}
	
	public void setUF(String UF) {
		this.UF = UF; 
	}	
}
