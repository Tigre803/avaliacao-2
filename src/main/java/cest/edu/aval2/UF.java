package cest.edu.aval2;

public class UF {
	
	private String sigla;
	private String descricao;
	
	//getters
	public String getSigla() {
		return sigla;
	}

	public String getDescricao() {
		return descricao;
	}
	//setters
	public void setSigla(String sigla) {
		this.sigla = sigla;
		
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}