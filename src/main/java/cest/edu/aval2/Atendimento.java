package cest.edu.aval2;

import java.sql.Date;

public class Atendimento {
	
	Date data;
	int numero;
	
	//construtor
	public Atendimento(Date data, int numero) {
		this.data = data;
		this.numero = numero; 
	}
	
	//getters
	public Date getData() {
		return data;
	}
	
	public int getNumero() {
		return numero;
		
	//setters	
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
}
	

