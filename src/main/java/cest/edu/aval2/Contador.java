package cest.edu.aval2;

public class Contador {
	
	private Contador Instance;
	private int proxNum;
	
	//construtor
	public Contador () {}
	
	//getters
	public Contador getInstance() {
		return Instance;
	}

	public int getProxNumero() {
		return proxNum;
	}
	
	//setters
	public void setInstance(Contador instance) {
		this.Instance = instance;
	}

	public void setProxNumero(int proxNumero) {
		this.proxNum = proxNumero;
	}
}
