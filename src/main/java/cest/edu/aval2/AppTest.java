package cest.edu.aval2;

import java.math.BigDecimal;

public class AppTest {
	
	public static void main(String[] args) {
		
		//FUNCIONÁRIO 
		Funcionário func = new Funcionário("Júlio Fernando da Silva","05145116801-50"
										,"9857805521", "Rua da Guerra, casa 30, Centro"
									,"juliofsilva5@hotmail.com", 20170, "Analista de sistemas");
		//atribuindo valor para salário
		BigDecimal slr = new BigDecimal(7250.00);
		func.setSalario(slr);
		//		
		System.out.println(" test > FUNCIONÁRIO < \n");
		System.out.println("Nome: " + func.getNome());
		System.out.println("CPF: " + func.getCPF());
		System.out.println("Telefone: " + func.getTelefone());
		System.out.println("Endereço: " + func.getEndereço());
		System.out.println("Email: " + func.getEmail());
		System.out.println("Cargo: " + func.getCargo());
		System.out.println("Salário: R$" + func.getSalario());
		System.out.println("Nº de matrícula na empresa: " + func.getMatricula() + "\n\n");
		
		
		//FORNECEDOR
		Fornecedor forn = new Fornecedor("Engesa", "9865486645", "Rua da Cerâmica, Parque industrial, nº 78", "engesagroup@hotmail.com", "84.960.960/0001-93");
		//				
		System.out.println(" test > FORNECEDOR < \n");
		System.out.println("Nome: " + forn.getNome());
		System.out.println("CNPJ: " + forn.getTelefone());
		System.out.println("Endereço: " + forn.getEndereço());
		System.out.println("Email: " + forn.getEmail());
		System.out.println("CNPJ: " + forn.getCNPJ() + "\n\n");
		
		
		//CONTADOR
		System.out.println("test < CONTADOR > ");
		Contador ctd = new Contador();
		//
		ctd.setProxNumero(22);
		System.out.println(ctd.getProxNumero());
		//
		ctd.setProxNumero(23);
		System.out.println(ctd.getProxNumero() + "\n\n");
		}
	}		