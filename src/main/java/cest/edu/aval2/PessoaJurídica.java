package cest.edu.aval2;

public class PessoaJurídica extends IPessoa{ 

	private String CNPJ;
		
	public PessoaJurídica(String nome, String telefone, String endereço, String email, String CNPJ) {
		super(nome, telefone, endereço, email);
		this.CNPJ = CNPJ;
	}

	//getters
	public String getCNPJ() {
		return CNPJ;
	}
		
	//setters
	public void setCNPJ(String CNPJ) {
		this.CNPJ = CNPJ;
	}
}
