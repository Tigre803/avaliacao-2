package cest.edu.aval2;

import java.math.BigDecimal;

public class Funcionário extends PessoaFísica{
	
	private int matricula;
	private BigDecimal salario;
	private String cargo;
	
	//construtor
	public Funcionário(String nome, String CPF, String telefone, String endereço
				, String email, int matricula, String cargo) {
		
		super(nome, CPF, telefone, endereço, email);
		this.matricula = matricula; 
		this.cargo = cargo;
	}
	
	//getters
	public int getMatricula( ) {
		return matricula;
	}
	
	public BigDecimal getSalario() {
		return salario;
	}
	
	public String getCargo() {
		return cargo;
	}	
	
	//setters	
	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}
		
	public void setSalario(BigDecimal salario) {
		this.salario = salario;	
	}
	
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
}
