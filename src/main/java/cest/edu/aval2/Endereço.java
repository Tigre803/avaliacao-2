package cest.edu.aval2;

public class Endereço extends Cidade {
	
	private String cidade;
	private String logradouro;
	
	//getters
	public String getCidade() {
		return cidade;
	}
	
	public String getLogradouro() {
		return logradouro;
	}
	
	//setters
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
}
 
